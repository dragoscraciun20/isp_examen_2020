package Examen;

public class K {
    private int ind;
    private String name;

    public K(){
        this.ind = 0;
        this.name = "first";
    }

    public K( int ind , String name ){
        this.ind = ind;
        this.name = name;

    }
}


class M extends K {
    private int indm;
    private B varb;

    public M(){
        this.indm = 0;
        this.varb = new B();
    }


    public M( int indm, int ind , String name ){
        super(ind,name);
        this.indm = indm;

    }
}

class B{
    private int varb;

    public B(){
        this.varb = 0;
    }

    public void metB(){
        System.out.println("Acesta este un obiect de tip B "+this.varb);
    }
}


class A{
    private int vara;
    private M objM;

    public A(){
        this.vara = 1;
        this.objM = new M();
    }

    public void metA(){
        System.out.println("Acesta este un obiect de tip A "+this.vara);
    }
}

class L{
    private int a;
    private M objM1;

    public L(){
        this.a = 1;
        this.objM1 = new M();
    }

    public void i( X x ){
        x = new X();
        System.out.println("Aici vom afisa obiectul de tip X");
    }
}


class X{
    private int varx;

    public X(){
        this.varx = 0;
    }
}


class Main{

    public static void main(String[] args) {
        K k1 = new K(1,"first");
        M m1 = new M();
        B b1 = new B();
        A a1 = new A();
        L l1 = new L();
        X x1 = new X();

        a1.metA();
        b1.metB();
        l1.i(x1);


        }


        }


