package Examen;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.*;


public class MyFrame extends JFrame implements ActionListener{

    private JTextField filenameTextField;
    private JTextField charactersTextField;
    private JButton butt;
    private JLabel flabel, clabel;


    public MyFrame()
    {
        super("Dragos Craciun App");
        initializeItems();
        this.setSize(new Dimension(400,400));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);

    }

    private void initializeItems()
    {
        this.setLayout(null);
        filenameTextField = new JTextField("File");
        charactersTextField = new JTextField("");
        butt = new JButton("Start");
        flabel = new JLabel("File Name");
        clabel = new JLabel("Character Number");
        butt.addActionListener(this);

        this.setLayout(new GridLayout(3, 2));
        this.getContentPane().add(flabel);
        this.getContentPane().add(filenameTextField);
        this.getContentPane().add(clabel);
        this.getContentPane().add(charactersTextField);
        this.getContentPane().add(butt);

    }


    @Override
    public void actionPerformed(ActionEvent arg0){

        File f;
        f = new File(filenameTextField.getText());

        try{

            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            int counter = 0;
            int c = 0;
            while((c = br.read()) != -1)
            {
                counter++;
            }
            br.close();
            fr.close();
            charactersTextField.setText(counter + "");
        }
        catch (IOException e){
            System.err.append("File might not exist");
        }

    }


}


 class MainClass{

    public static void main(String[] args){
        MyFrame f = new MyFrame();

    }

}
